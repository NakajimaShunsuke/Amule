require "sqlite3"
require "active_record"

ActiveRecord::Base.establish_connection(
    "adapter" =>"sqlite3",
    "database" => "./database/amuledatabase"
)

class User < ActiveRecord::Base
end

class Timetable < ActiveRecord::Base
end

class Myschedule < ActiveRecord::Base
end

class Shopping < ActiveRecord::Base
end