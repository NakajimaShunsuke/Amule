require "sinatra/base"
require "./models/databasemodel.rb"

#アカウント保持するプログラムを記述していない

#メイソン画面を表示するドメイン
class Schedule < Sinatra::Base
    get '/' do
        @username = User.pluck(:username)
        @today = "2018-01-20"
        @timetable = Timetable.where(username: "中島駿介",today: @today).pluck(:username,:tableA,:tableB,:tableC,:tableD,:tableE,:tableF,:tableG,:tableH,:tableI,:tableJ,:tableK,:tableL,:tableM,:tableN,:tableO,)
        
        erb :schedule
    end
end

#自分のスケジュールの表示
class MyscheduleTable < Sinatra::Base
    get '/' do
        @username = User.pluck(:username)
        @today = "2018-01-20"
        @timetable = Timetable.where(username: "中島駿介",today: @today).pluck(:username,:tableA,:tableB,:tableC,:tableD,:tableE,:tableF,:tableG,:tableH,:tableI,:tableJ,:tableK,:tableL,:tableM,:tableN,:tableO,)
        @mytable = Myschedule.where(username: "中島駿介",today: @today).pluck(:username,:timeA,:timeB,:timeC,:timeD,:timeE,:timeF,:timeG,:timeH,:timeI,:timeJ,:timeK,:timeL,:timeM,:timeN,:timeO,)

        erb :myschedule
    end
end

#購入履歴を表示する
class Shoppinglist < Sinatra::Base
    get '/' do
        @username = User.pluck(:username)
        @today = "2018-01-20"
        @shoppinglist = Shopping.where(username: "中島駿介",day: @today).pluck(:shop).uniq
        @shopping = {}
        @totalfee ={}
        @daytotal = 0

        @shoppinglist.each do |shop|
            @shopping.store(shop,Shopping.where(username: "中島駿介",day: @today,shop: shop).pluck(:goods,:count,:unitprice))
        end

        #合計金額処理
        @shoppinglist.each do |shop|
            price = 0
            @shopping[shop].each do |goods|
                price += goods[1] * goods[2]
            end
            @daytotal += price
            @totalfee.store(shop,price)
        end

        erb :shopping
    end
end

#QLコードを読み込んでアトラクションを追加させるドメイン
class AttractionsAdd < Sinatra::Base
    get '/' do
        @paradename = params[:paradename]
        @table = params[:table]
        @time = params[:time]

        erb :scheduleadd
    end

    post '/' do
        @paradename = params[:paradename]
        @table = params[:table]
        @time = params[:time]
        @username = User.where(username: "中島駿介").pluck(:username)

        @today = "2018-01-20"
        @timetable = Timetable.where(username: "中島駿介",today: @today).first
        @paradetime = Myschedule.where(username: "中島駿介",today: @today).first

        if @table == "tableA"
            @timetable.tableA = @paradename
            @paradetime.timeA = @time
        elsif @table == "tableB"
            @timetable.tableB = @paradename
            @paradetime.timeB = @time
        elsif @table == "tableC"
            @timetable.tableC = @paradename
            @paradetime.timeC = @time
        elsif @table == "tableD"
            @timetable.tableD = @paradename
            @paradetime.timeD = @time
        elsif @table == "tableE"
            @timetable.tableE = @paradename
            @paradetime.timeE = @time
        elsif @table == "tableF"
            @timetable.tableF = @paradename
            @paradetime.timeF = @time
        elsif @table == "tableG"
            @timetable.tableG = @paradename
            @paradetime.timeG = @time
        elsif @table == "tableH"
            @timetable.tableH = @paradename
            @paradetime.timeH = @time
        elsif @table == "tableI"
            @timetable.tableI = @paradename
            @paradetime.timeI = @time
        elsif @table == "tableJ"
            @timetable.tableJ = @paradename
            @paradetime.timeJ = @time
        elsif @table == "tableK"
            @timetable.tableK = @paradename
            @paradetime.timeK = @time
        elsif @table == "tableL"
            @timetable.tableL = @paradename
            @paradetime.timeL = @time
        elsif @table == "tableM"
            @timetable.tableM = @paradename
            @paradetime.timeM = @time
        elsif @table == "tableN"
            @timetable.tableN = @paradename
            @paradetime.timeN = @time
        elsif @table == "tableO"
            @timetable.tableO = @paradename
            @paradetime.timeO = @time
        else
            redirect '/'
        end

        @timetable.save
        @paradetime.save

        redirect '/'
    end
end

class ShoppingAdd < Sinatra::Base

    get '/' do
        @goods = Rack::Utils.parse_query(@env['rack.request.query_string'])
        erb :shoppingadd
    end

    post '/' do

        @goods = Rack::Utils.parse_query(@env['rack.request.form_vars'])
        @username = User.where(username: "中島駿介").pluck(:username)
        @today = "2018-01-20"

        if @goods['goods'].class == Array
            for num in 0..@goods['goods'].length-1 do
                Shopping.create(:username => @username[0], :day => @today,:shop => @goods['shop'],:goods => @goods['goods'][num],:count =>@goods['count'][num],:unitprice => @goods['unitprice'][num])
            end
        else
            Shopping.create(:username => @username[0], :day => @today,:shop => @goods['shop'],:goods => @goods['goods'], :count =>@goods['count'],:unitprice => @goods['unitprice'])
        end

        redirect '/Shopping'

    end
end

class Test < Sinatra::Base
    get '/' do
        

    end
end
