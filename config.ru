require "sinatra/base"
require "./app.rb"

map('/') { run Schedule }
map('/Myschedule') { run MyscheduleTable }
map('/Shopping'){ run Shoppinglist }
map('/AttractionsAdd') { run AttractionsAdd }
map('/ShoppingAdd') { run ShoppingAdd }
map('/test') { run Test }
