CREATE TABLE users (
    mailaddress TEXT PRIMARY KEY,
    username TEXT,
    pass TEXT NOT NULL
);

CREATE TABLE timetables (
    timetablesNo INTEGER PRIMARY KEY AUTOINCREMENT,
    username TEXT,
    today TEXT,
    tableA TEXT,
    tableB TEXT,
    tableC TEXT,
    tableD TEXT,
    tableE TEXT,
    tableF TEXT,
    tableG TEXT,
    tableH TEXT,
    tableI TEXT,
    tableJ TEXT,
    tableK TEXT,
    tableL TEXT,
    tableM TEXT,
    tableN TEXT,
    tableO TEXT
);

CREATE TABLE myschedules (
    myscheduleNo INTEGER PRIMARY KEY AUTOINCREMENT,
    username TEXT,
    today TEXT,
    timeA TEXT,
    timeB TEXT,
    timeC TEXT,
    timeD TEXT,
    timeE TEXT,
    timeF TEXT,
    timeG TEXT,
    timeH TEXT,
    timeI TEXT,
    timeJ TEXT,
    timeK TEXT,
    timeL TEXT,
    timeM TEXT,
    timeN TEXT,
    timeO TEXT
);

CREATE TABLE shoppings(
    shoppings INTEGER PRIMARY KEY AUTOINCREMENT,
    username TEXT,
    day TEXT,
    shop Text,
    goods TEXT,
    count INTEGER,
    unitprice INTEGER
);